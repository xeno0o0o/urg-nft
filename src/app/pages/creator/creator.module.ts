import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { CreatorComponent } from './creator.component';

const routes: Routes = [
  {
    path: '',
    component: CreatorComponent,
  },
];

@NgModule({
  declarations: [
    CreatorComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ],
  exports: [RouterModule]
})
export class CreatorModule { }
