import { AfterViewInit, ChangeDetectorRef, Component, ElementRef, OnInit, ViewChild, HostListener } from '@angular/core';
import { EventService } from 'src/app/services/event.service';
import {
  SwiperComponent, SwiperDirective, SwiperConfigInterface,
  SwiperScrollbarInterface, SwiperPaginationInterface
} from 'ngx-swiper-wrapper';
import { EndpointService } from 'src/app/services/endpoint.service';

declare var $: any;
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit, AfterViewInit {
  @ViewChild('currentWrap', { static: false }) currentWrap: ElementRef | undefined;

  nftObj: any;
  nftList: any = [];
  newNFT: any = [];
  animationNFT: any = [];
  mongolNFT: any = [];
  innerWidth: any;
  movieNFT: any = [];
  esportNFT: any = [];
  IMG_PATH: string = '../../../assets/images/';

  markStyle: any;
  urgDaxVal: any;

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.resizeSlide()
  }

  public config: SwiperConfigInterface = {
    slidesPerView: 1,
    navigation: true,
    autoplay: {
      delay: 3000
    }
  };

  constructor(
    private eventService: EventService,
    private cdr: ChangeDetectorRef,
    private endpointService: EndpointService
  ) { }

  ngOnInit(): void {
    this.nftObj = this.eventService.getNFTList();
    if (this.nftObj === null || this.nftObj === undefined) {
      this.getGifts();
    } else {
      this.nftList = this.nftObj.resolved.slice(0, 4);
      this.newNFT = this.nftObj.resolved.filter((item: any) => item.type === 'new');
      this.animationNFT = this.nftObj.resolved.filter((item: any) => item.catelogy === 'animation').slice(0, 4);
      this.movieNFT = this.nftObj.resolved.filter((item: any) => item.catelogy === 'movie');
      this.esportNFT = this.nftObj.resolved.filter((item: any) => item.catelogy === 'esport');
    }
    this.getDaxVal();
  }

  ngAfterViewInit() {
    this.resizeSlide();
    this.cdr.detectChanges();
  }

  getGifts() {

  }

  resizeSlide() {
    let resize = ((this.currentWrap?.nativeElement.offsetWidth * 2.78) / 10);
    this.markStyle = resize;
  }

  getDaxVal() {
    const data = {
      "query": "query ActivePairs {\n  sys_pair(where: {is_active: {_eq: true}}, order_by: {id: asc}) {\n    id\n    symbol\n    base_max_size\n    base_min_size\n    base_tick_size\n    quote_max_size\n    quote_min_size\n    quote_tick_size\n    baseAsset {\n      id\n      code\n      name\n      scale\n      is_crypto\n      __typename\n    }\n    quoteAsset {\n      id\n      code\n      name\n      scale\n      __typename\n    }\n    price {\n      last_price\n      last_price_dt\n      __typename\n    }\n    stats24 {\n      high\n      low\n      change24h\n      vol\n      __typename\n    }\n    __typename\n  }\n}\n"
    }
    this.endpointService.getURGbyDax(data).subscribe((res: any) => {
      if (res.data) {
        const urgData = res.data.sys_pair.find((el: any) => el.symbol === 'URGMNT');
        this.urgDaxVal = urgData.price.last_price / 100;
      }
    });
  }

}
