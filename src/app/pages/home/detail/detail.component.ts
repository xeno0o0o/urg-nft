import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MessageService } from 'primeng/api';
import { EventService } from 'src/app/services/event.service';

@Component({
  selector: 'app-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss']
})
export class DetailComponent implements OnInit {


  endDate: string = '2021-12-22 09:00:00';

  likeCount: number = 0;
  viewCount: number = 0;
  shareCount: number = 0;

  isLike: boolean = false;
  isShare: boolean = false;
  nftObj: any;
  nftList: any = [];
  cardId: any;
  nft: any = {};
  IMG_PATH: string = '../../../assets/images/';

  constructor(
    private eventService: EventService,
    private route: ActivatedRoute,
    private messageService: MessageService
  ) {
    this.cardId = this.route.snapshot.paramMap.get('id');
  }

  ngOnInit(): void {
    this.nftObj = this.eventService.getNFTList();
    if (this.nftObj === null || this.nftObj === undefined) {
      this.getGifts();
    } else {
      this.nftList = this.nftObj.resolved;
      console.log(this.nftList)
      this.nft = this.nftList.find((item: any) => item.id === +this.cardId);
      this.likeCount = this.nft.like;
      this.shareCount = this.nft.share;
      this.viewCount = this.nft.view;
    }
  }

  onLike() {
    this.isLike = !this.isLike;
    this.likeCount = this.isLike === true ? this.likeCount + 1 : this.likeCount - 1;
  }

  onShare() {
    this.isShare = !this.isShare;
    this.shareCount = this.isShare ? this.shareCount + 1 : this.shareCount - 1;
  }

  getGifts() {

  }

  copyAlert() {
    this.messageService.add({
      severity: 'success',
      summary: 'Амжилттай',
      detail: 'Текстийг амжилттай хуулсан.'
    });
  }
}
