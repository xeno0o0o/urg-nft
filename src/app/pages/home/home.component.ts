import { Component, OnInit } from '@angular/core';
import { SwiperComponent } from "swiper/angular";
import SwiperCore, { Swiper, Virtual } from 'swiper';
import { ActivatedRoute } from '@angular/router';
import { EventService } from 'src/app/services/event.service';

SwiperCore.use([Virtual]);
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  constructor(
    private route: ActivatedRoute,
    private eventService: EventService
  ) { }

  ngOnInit(): void {
    this.route.data
      .subscribe((data: any) => {
        this.eventService.setNFTList(data);
      });
  }

}
