import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { EventService } from 'src/app/services/event.service';


@Component({
  selector: 'app-market',
  templateUrl: './market.component.html',
  styleUrls: ['./market.component.scss']
})
export class MarketComponent implements OnInit {

  nftObj: any;
  nftList: any = [];
  newNFT: any = [];
  animationNFT: any = [];
  allNFT: any = [];
  mongolNFT: any = [];
  movieNFT: any = [];
  IMG_PATH: string = '../../../assets/images/';
  selectedIndex: number = 0;
  marketType: any;
  cities: any = [];
  selectedCity: any;

  constructor(
    private eventService: EventService,
    private route: ActivatedRoute
  ) {
    this.marketType = this.route.snapshot.paramMap.get('type');

    this.cities = [
      { name: 'New York', code: 'NY' },
      { name: 'Rome', code: 'RM' },
      { name: 'London', code: 'LDN' },
      { name: 'Istanbul', code: 'IST' },
      { name: 'Paris', code: 'PRS' }
    ];
  }

  ngOnInit(): void {
    this.nftObj = this.eventService.getNFTList();
    if (this.nftObj === null || this.nftObj === undefined) {
      this.getGifts();
    } else {
      this.showRouteNfts();
    }
  }
  getGifts() {

  }

  selectTab(type: any, index: any) {
    this.selectedIndex = +index;
    this.marketType = type;

    if (type === 'all') {
      this.nftList = this.allNFT;
      return;
    }
    this.nftList = this.allNFT.filter((item: any) => item.catelogy === type && !item.hasOwnProperty('parentId'));
  }

  showRouteNfts() {
    this.nftList = this.nftObj.resolved;
    this.allNFT = this.nftList;

    if (this.marketType !== 'all') {
      this.nftList = this.nftList.filter((item: any) => item.catelogy === this.marketType && !item.hasOwnProperty('parentId'));
      this.selectedIndex = this.marketType === 'esport' ? 1 : this.marketType === 'animation' ? 2 : 3;
    } else {
      this.nftList = this.nftList.filter((item: any) => !item.hasOwnProperty('parentId'));
    }
  }
}
