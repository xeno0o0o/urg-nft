import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { SwiperModule } from 'ngx-swiper-wrapper';
import { SWIPER_CONFIG } from 'ngx-swiper-wrapper';
import { SwiperConfigInterface } from 'ngx-swiper-wrapper';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClipboardModule } from 'ngx-clipboard';

import { HomeComponent } from './home.component';
import { MainComponent } from './main/main.component';
import { DetailComponent } from './detail/detail.component';
import { MarketComponent } from './market/market.component';
import { HomeResolver } from 'src/app/services/Resolver/home.resolver';
import { MessageService } from 'primeng/api';
import { ToastModule } from 'primeng/toast';

import { SharedModule } from 'src/app/shared/shared.module';
const routes: Routes = [
  {
    path: '',
    redirectTo: 'main',
    pathMatch: 'full',
  },
  {
    path: '',
    component: HomeComponent,
    resolve: { resolved: HomeResolver },
    children: [
      { path: 'main', component: MainComponent },
      { path: 'market/:type', component: MarketComponent },
      { path: 'detail/:id', component: DetailComponent }
    ]
  }
];

const DEFAULT_SWIPER_CONFIG: SwiperConfigInterface = {
  direction: 'horizontal',
  slidesPerView: 'auto'
};

@NgModule({
  declarations: [
    HomeComponent,
    MainComponent,
    DetailComponent,
    MarketComponent,
  ],
  imports: [
    CommonModule,
    SwiperModule,
    FormsModule,
    ReactiveFormsModule,
    ClipboardModule,
    ToastModule,
    SharedModule,
    RouterModule.forChild(routes),
  ],
  providers: [
    {
      provide: SWIPER_CONFIG,
      useValue: DEFAULT_SWIPER_CONFIG
    },
    MessageService
  ],
  exports: [RouterModule]
})
export class HomeModule { }
