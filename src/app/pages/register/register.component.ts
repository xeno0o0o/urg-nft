import { Component, OnInit } from '@angular/core';

import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  regex1: boolean = false;
  regex2: boolean = false;
  regex3: boolean = false;
  regex4: boolean = false;

  termDialog = false;
  isSame: boolean = true;
  secondPass: any;

  registForm: FormGroup = new FormGroup({
    phone: new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(8)]),
    password: new FormControl('', [
      Validators.required,
      Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$#^!%*&?&])[A-Za-z0-9\d$@$!#%*&?&].{8,}')]),
  });

  constructor() { }

  ngOnInit(): void {
  }

  regexCheck() {
    let pass = this.registForm.controls['password'].value;
    const pattern1 = new RegExp('[A-Za-z\d$@$!%*?&].{8,}'); // 8 urttai
    const pattern2 = new RegExp('(?=.*[A-Z])'); // tom useg
    const pattern3 = new RegExp('(?=.*[0-9])'); // too
    const pattern4 = new RegExp('(?=.*[$@$!%*#?&])'); // temdeg

    this.regex1 = pattern1.test(pass);
    this.regex2 = pattern2.test(pass);
    this.regex3 = pattern3.test(pass);
    this.regex4 = pattern4.test(pass);
  }

  checkSamePassword() {
    this.isSame = (this.secondPass === this.registForm.controls['password'].value) ? false : true;
    return this.isSame;
  }

  showTerm() {
    this.termDialog = true;
  }
  hideTerm() {
    this.termDialog = false;
  }
}
