import { Injectable } from '@angular/core';
import { HttpClient, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { Router } from '@angular/router';
import { catchError } from 'rxjs/operators';
import { AuthService } from 'src/app/services/auth.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
  constructor(
    private http: HttpClient,
    private router: Router,
    private authService: AuthService,
  ) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<any> {
    const headersConfig = {
      Accept: 'application/json',
    } as any;

    if (req.headers.get('Content-Type')) {
      headersConfig['Content-Type'] = req.headers.get('Content-Type');
    }
    if (req.headers.get('responseType')) {
      headersConfig[`responseType`] = req.headers.get('responseType');
    }
    if (req.headers.get('Accept')) {
      headersConfig[`Accept`] = req.headers.get('Accept');
    }
    const token = localStorage.getItem('access_token');
    if (token) {
      headersConfig.Authorization = `Bearer ${token}`;
    }

    const request = req.clone({ setHeaders: headersConfig });
    return next.handle(request)
      .pipe(
        catchError((error: any) => {
          if (error.status === 401) {
            this.authService.logout();
            return throwError(error);
          } else if (error.status === 403) {
            this.router.navigate(['error'],
              { queryParams: { code: '403', status: 'Хандалт байхгүй', message: 'Танд хандах эрх байхгүй байна.' } });
            return throwError(error);
          } else {
            return throwError(error);
          }
        }) as any
      );
  }
}
