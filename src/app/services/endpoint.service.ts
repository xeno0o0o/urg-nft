import { HttpBackend, HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { throwError } from 'rxjs';

import { Observable } from 'rxjs/internal/Observable';
import { catchError } from 'rxjs/operators';
import { API_BUY_CHECK, API_BUY_GIFT, API_FILE_DOWNLOAD, API_FORGOT, API_GIFT, API_KYC_BACK, API_KYC_FRONT, API_KYC_SELFIE, API_ME, API_OWNED_GIFT, API_PROFILE_CHANGE_PASS, API_REGIST_CONFIRM, API_SEND_GIFT, API_TAN_AGAIN, API_USER_CONFIRM_INFO, API_USER_URG_BALANCE, API_USE_GIFT, API_WALLET_INT, API_WITHDRAW, API_WITHDRAW_HISTORY, API_WITHDRAW_TAN, URG_DAX_VAL } from '../shared/uri.constants';
import { ApiService } from './api.service';

@Injectable({ providedIn: 'root' })
export class EndpointService {
  constructor(
    private api: ApiService,
    private http: HttpClient,
    private handler: HttpBackend,
  ) { }

  public formatErrors(error: any): any {
    return throwError(error && error.error ? error.error : error);
  }

  getUserInfo() {
    return this.api.get(API_ME);
  }

  tanAgain(phoneNo: string): Observable<any> {
    return this.api.post(API_TAN_AGAIN, phoneNo);
  }

  registConfirm(phone: string, tan: string): Observable<any> {
    const data = {
      phoneNo: phone,
      tanCode: tan
    }
    return this.api.post(API_REGIST_CONFIRM, data);
  }

  forgetPassword(phone: any) {
    return this.api.post(API_FORGOT, phone);
  }

  getGift() {
    return this.api.get(API_GIFT);
  }

  fileDownload(filePath: string): Observable<any> {
    return this.http.get(API_FILE_DOWNLOAD + filePath, { observe: 'response', responseType: 'blob' });
  }

  kycFront(data: any) {
    return this.api.postImage(API_KYC_FRONT, data);
  }

  kycBack(data: any) {
    return this.api.postImage(API_KYC_BACK, data);
  }

  kycSelfie(data: any) {
    return this.api.postImage(API_KYC_SELFIE, data);
  }

  getUserUrgBalance(): Observable<any> {
    return this.api.get(API_USER_URG_BALANCE);
  }

  profileChangePass(profileModal: any): Observable<any> {
    return this.api.post(API_PROFILE_CHANGE_PASS, profileModal);
  }

  confirmUserInfo(userInfo: any) {
    return this.api.post(API_USER_CONFIRM_INFO, userInfo);
  }

  getWalletIns() {
    return this.api.get(API_WALLET_INT);
  }

  getOwnedGift() {
    return this.api.get(API_OWNED_GIFT);
  }

  useGiftCard(itemId: any) {
    return this.api.get(API_USE_GIFT, itemId);
  }

  sendGiftCard(obj: any) {
    return this.api.get(API_SEND_GIFT, obj)
  }

  buyGiftCard(obj: any) {
    return this.api.get(API_BUY_GIFT, obj);
  }

  buyGiftCheck(queryId: any) {
    return this.api.get(API_BUY_CHECK, queryId)
  }

  sendWithDraw(withDrawObj: any) {
    return this.api.post(API_WITHDRAW, withDrawObj);
  }

  withdrawTan() {
    return this.api.get(API_WITHDRAW_TAN);
  }

  getWithdrawHistory() {
    return this.api.get(API_WITHDRAW_HISTORY);
  }

  getURGbyDax(body: any) {
    // let headers = new HttpHeaders();
    // headers = headers.append('Content-Type', 'application/json');
    this.http = new HttpClient(this.handler);
    return this.http
      .post(URG_DAX_VAL, body, {});

  }
}
