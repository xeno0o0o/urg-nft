import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { environment } from 'src/environments/environment';
import { ApiService } from './api.service';
import { EndpointService } from './endpoint.service';

const AUTH_URL = environment.url;

@Injectable({ providedIn: 'root' })
export class FileService {
  constructor(
    private api: ApiService,
    private endpointService: EndpointService
  ) { }

}
