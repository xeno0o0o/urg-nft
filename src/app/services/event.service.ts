import { Injectable } from "@angular/core";
import { Subject } from "rxjs";


@Injectable({ providedIn: 'root' })
export class EventService {
  public giftsEmit = new Subject();
  public nftList: any = [];

  setGifts(giftList: any) {
    this.giftsEmit.next(giftList);
  }

  setNFTList(data: any) {
    this.nftList = data;
  }
  getNFTList() {
    return this.nftList;
  }
}
