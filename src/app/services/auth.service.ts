import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from "@angular/router";
import { Observable, of, throwError } from "rxjs";
import { catchError, tap } from 'rxjs/operators';
import { WEB_ACCESS, API_FORGOT, API_LOGIN, API_REGIST, AUTH_URL } from "../shared/uri.constants";
import { UserService } from "./user.service";
import { ApiService } from "./api.service";


const TOKEN_DATA = 'grant_type=#g';
const LOGIN_DATA = 'username=#u&password=#p&grant_type=#g';


@Injectable({ providedIn: 'root' })
export class AuthService implements AuthService {

  constructor(
    private http: HttpClient,
    private router: Router,
    private userService: UserService,
    private apiService: ApiService
  ) { }

  public getAccessToken(): Observable<string | null> {
    return of(localStorage.getItem('access_token'));
  }

  public refreshToken(): void {
  }

  public getToken(): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Basic ' + btoa(WEB_ACCESS),
      })
    };
    let formData = TOKEN_DATA.replace('#g', 'client_credentials');
    return this.http.post(AUTH_URL, formData, httpOptions).pipe(
      tap((tokens: any) => {
        for (const i in tokens) {
          localStorage.setItem(i, tokens[i]);
        }
      }),
      catchError((error) => throwError(error.error))
    );
  }

  public login(form: { username: string, password: string }): Observable<any> {
    let formData = LOGIN_DATA.replace('#u', form.username);
    formData = formData.replace('#p', form.password);
    formData = formData.replace('#g', 'password');
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Basic ' + btoa(WEB_ACCESS),
      })
    };
    return this.http.post(API_LOGIN, formData, httpOptions).pipe(
      tap((tokens: any) => {
        this.saveAccessData(tokens);
        this.userService.setUser(tokens[`access_token`]);
      }),
      catchError((error) => throwError(error.error))
    );
  }

  public logout(): void {
    localStorage.clear();
    this.userService.logout();
    this.router.navigate(['/home']).then();
  }

  public saveAccessData(accessData: any): void {
    for (const i in accessData) {
      localStorage.setItem(i, accessData[i]);
    }
  }

  public regist(form: { phone: string, password: string }) {
    const data = {
      phoneNo: form.phone,
      pwd: form.password
    };
    return this.apiService.post(API_REGIST, data).pipe(
      tap((res: any) => {
        if (res && res.status === 0) {
        }
      }),
      catchError((error) => throwError(error.error))
    );
  }

  public forgotPassword(phone: string) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Basic ' + btoa(WEB_ACCESS),
      })
    };
    const data = {
      phoneNo: phone
    }
    return this.http.post(API_FORGOT, data, httpOptions).pipe(
      tap((res: any) => { }),
      catchError((error) => throwError(error.error))
    );
  }
}
