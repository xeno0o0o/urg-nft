import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

@Injectable({ providedIn: 'root' })
export class ApiService {
  constructor(private http: HttpClient) { }

  public formatErrors(error: any): any {
    return throwError(error && error.error ? error.error : error);
  }

  get(
    path: string,
    params: HttpParams = new HttpParams()
  ): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    const url = path.includes('http') || path.includes('https') ? path : `${environment.url}${path}`;
    return this.http
      .get(url, { params, headers })
      .pipe(catchError(this.formatErrors));
  }

  getImage(
    path: string,
    params: HttpParams = new HttpParams()
  ): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'image/png');
    headers = headers.append('responseType', 'blob');

    const url = path.includes('http') || path.includes('https') ? path : `${environment.url}${path}`;
    return this.http
      .get(url, { params, headers })
      .pipe(catchError(this.formatErrors));
  }

  put(path: string, body: any, params?: HttpParams): Observable<any> {
    const headers = new HttpHeaders().append(
      'Content-Type',
      'application/json'
    );
    return this.http
      .put(`${environment.url}${path}`, body, { headers, params })
      .pipe(catchError(this.formatErrors));
  }

  patch(path: string, body: any, params?: HttpParams): Observable<any> {
    const headers = new HttpHeaders().append(
      'Content-Type',
      'application/application/json'
    );
    return this.http
      .patch(`${environment.url}${path}`, body, { headers, params })
      .pipe(catchError(this.formatErrors));
  }

  post(path: string, body: any, params?: any): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    if (localStorage.getItem('access_token')) {
      headers = headers.append(
        'Authorization',
        `Bearer ${localStorage.getItem('access_token')}`
      );
    }
    return this.http
      .post(`${environment.url}${path}`, body, { params, headers })
      .pipe(catchError(this.formatErrors));
  }

  postImage(path: string, body: any, params?: any): Observable<any> {
    let headers = new HttpHeaders();

    if (localStorage.getItem('access_token')) {
      headers = headers.append(
        'Authorization',
        `Bearer ${localStorage.getItem('access_token')}`
      );
    }
    return this.http
      .post(`${environment.url}${path}`, body, { params, headers, reportProgress: false, observe: 'events' })
      .pipe(catchError(this.formatErrors));
  }

  delete(path: string, params?: any): Observable<any> {
    let headers = new HttpHeaders();
    headers = headers.append('Content-Type', 'application/json');
    if (localStorage.getItem('access_token')) {
      headers = headers.append(
        'Authorization',
        `Bearer ${localStorage.getItem('access_token')}`
      );
    }
    return this.http
      .delete(`${environment.url}${path}`, { params, headers })
      .pipe(catchError(this.formatErrors));
  }

}
