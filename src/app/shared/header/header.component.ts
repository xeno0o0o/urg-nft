import { Component, OnInit, HostListener, ElementRef, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  isDark = true;
  innerWidth: any;
  isMobile = false;
  collapseBtn = false;
  isViewSearch = false;

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.innerWidth = window.innerWidth;
    if (this.innerWidth <= 1091) {
      this.isMobile = true;
    } else {
      this.isMobile = false;
    }
  }

  constructor() { }

  ngOnInit(): void {
    this.onResize();
    this.darkMode();
  }

  darkMode() {
    this.isDark = !this.isDark;
    if(this.isDark) {
      this.isDark = true;
      document.body.classList.remove('dark');
    } else {
      this.isDark = false;
      document.body.classList.add('dark');
    }
  }

  toggleMegaMenu() {
    this.collapseBtn = !this.collapseBtn;
    this.isViewSearch = false;
    if (this.collapseBtn) {
      this.collapseBtn = true;
      document.body.classList.add('visible');
    } else {
      this.collapseBtn = false;
      document.body.classList.remove('visible');
    }
  }

  searchForm() {
    this.collapseBtn = false;
    this.isViewSearch = !this.isViewSearch;
    if (this.isViewSearch) {
      this.isViewSearch = true;
      document.body.classList.add('visible');
    } else {
      this.isViewSearch = false;
      document.body.classList.remove('visible');
    }
  }

  closeMenu() {
    this.collapseBtn = false;
    this.isViewSearch = false;
    document.body.classList.remove('visible');
  }
}
