import { environment } from "src/environments/environment";

export const HOST = environment.url;
export const WEB_ACCESS = `web:QwGg^u/'N9-fS!-M`;
export const BLOCK_DEC = 1e+18;
export const AUTH_URL = HOST + '/uaa/oauth/token';

export const IMG_PATH = HOST + '/file/v1/d/'

export const URG_DAX_VAL = 'https://api.dax.mn/v1/graphql';

export const API_LOGIN = HOST + '/uaa/oauth/token';
export const API_REGIST = '/app/v1/user/register';
export const API_FORGOT = '/app/v1/user/forgot-pwd';

export const API_TAN_AGAIN = '/app/v1/user/register-tan-again';
export const API_REGIST_CONFIRM = '/app/v1/user/register-confirm';
export const API_PROFILE_CHANGE_PASS = '/app/v1/user/change-pwd';
export const API_ME = '/app/v1/user/me';
export const API_USER_URG_BALANCE = '/app/v1/wallet/urgt';
export const API_USER_CONFIRM_INFO = '/app/v1/user/change-info';

export const API_WALLET_INT = '/app/v1/wallet/txn-list';

export const API_GIFT = '/app/v1/ecommerce/list?page=0&size=100';
export const API_FILE_DOWNLOAD = HOST + '/file/v1/d/';

export const API_OWNED_GIFT = '/app/v1/ecommerce/my-owned?page=0&size=100';
export const API_USE_GIFT = '/app/v1/ecommerce/use-gc';
export const API_SEND_GIFT = '/app/v1/ecommerce/send-gc';
export const API_BUY_GIFT = '/app/v1/ecommerce/buy-gc';
export const API_BUY_CHECK = '/app/v1/ecommerce/buy-gc-status';

export const API_KYC_FRONT = '/file/v1/user/upload-id-front';
export const API_KYC_BACK = '/file/v1/user/upload-id-back';
export const API_KYC_SELFIE = '/file/v1/user/upload-selfie-with-id'

export const API_WITHDRAW = '/app/v1/wallet/withdraw';
export const API_WITHDRAW_TAN = '/app/v1/wallet/withdraw-request';
export const API_WITHDRAW_HISTORY = '/app/v1/wallet/withdraw-history?page=0&size=100';





