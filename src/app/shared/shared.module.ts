import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { CountdownModule } from 'ngx-countdown';

import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { CountdownComponent } from './countdown/countdown.component';

const components = [
  HeaderComponent, FooterComponent, CountdownComponent
];

@NgModule({
  declarations: [...components],
  imports: [
    CommonModule,
    CountdownModule,
    RouterModule,
  ],
  exports: [...components],
})
export class SharedModule { }
